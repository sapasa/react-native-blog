After cloning, ensure project runs. It should load an empty screen with fab buttons. There may be yellow warnings, those will be resolved after these next steps

Using terminal from project root run these commands

``` 
cd ..
mkdri jsonserver
```

These should make an external folder

``` 
cd jsonserver 
npm initi 
npm install json-server ngrok  
code.

``` 


create a db.json file, in this file put in this code

```
{
    "blogs": []
}
```

and change your package.json to this

```
{
  "name": "jsonserver",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "db": "json-server -w db.json",
    "tunnel": "ngrok http 3000"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "json-server": "^0.16.1",
    "ngrok": "^3.2.7"
  }
}

```

run

```
npm run db

npm run tunnel

```



It will run the json server and tunnel makes visible to outside world using a forward port. 


It should show 2 forwarding ports, copy the first on and plaste the url back in the jsonServer.js here: react-native-blog\src\api\jsonServer.js

Save and run app again. It should run without yellow warnings




to this 




