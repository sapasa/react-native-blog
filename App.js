import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import IndexScreen from './src/screens/IndexScreen';
import React from 'react';
import { Provider } from './src/context/BlogContext';
import ShowScreen from './src/screens/ShowScreen';
import CreateScreen from './src/screens/CreateScreen';
import Icon from 'react-native-vector-icons';
import EditScreen from './src/screens/EditScreen';
import LikedScreen from './src/screens/LikedScreen';
import ArchivedScreen from './src/screens/ArchivedScreen';

const navigator = createStackNavigator({
    Index: IndexScreen,
    Show: ShowScreen,
    Create: CreateScreen,
    Edit: EditScreen,
    Liked: LikedScreen,
    Archived: ArchivedScreen
  
  
  },
    {
        initialRouteName: 'Index',
        defaultNavigationOptions: {
            title: 'Blogs',
            headerShown: false,
            animationEnabled: false,

        }
    });

    const App = createAppContainer(navigator);

    export default () => {
      return <Provider>
        <App/>
      </Provider>
    };