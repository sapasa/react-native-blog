import React, { useState } from 'react';
import { Image as ReactImage, View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export const Tags = (props) => {




    function Item({ id, title, onSelect, color }) {

        switch (color) {

            case 'red':
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.red, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );
            case 'orange':
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.orange, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );

            case 'blue':
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.blue, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );

            default:
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.red, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );
        }


    }



    return (
        <FlatList
            style={{ paddingBottom: 20 }}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={props.data}
            renderItem={({ item }) => (
                <Item
                    id={item.id}
                    title={item.title}
                    color={item.color}
                />
            )}
            keyExtractor={item => item.id}
        />
    );
};

export const EditTags = () => {

    //api retrieve all tags
    const tags = [
        {
            id: 1,
            title: 'important',
            color: 'red'
        },
        {
            id: 2,
            title: 'to-do',
            color: 'orange'
        },
        {
            id: 3,
            title: 'study',
            color: 'blue'
        },

    ]


    function Item({ id, title, color, selected }) {


        switch (color) {

            case 'red':
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.red , TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );
            case 'orange':
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.orange, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );

            case 'blue':
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.blue, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );

            default:
                return (
                    <TouchableOpacity>
                        <Text style={[TagStyle.red, TagStyle.label]}>{title}</Text>
                    </TouchableOpacity>
                );
        }


    }



    return (
        <FlatList
            style={{ paddingBottom: 20 }}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={tags}
            renderItem={({ item }) => (
                <Item
                    id={item.id}
                    title={item.title}
                    color={item.color}
                />
            )}
            keyExtractor={item => item.id}
        />
    );
};


const TagStyle = StyleSheet.create({
    container: {
        backgroundColor: '#22b5ff',
        borderRadius: 30,
        padding: 15,
        alignSelf: 'flex-end'
    },
    label: {
        fontSize: 15,
        borderRadius: 20,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 15,
        paddingRight: 15,
        marginRight: 20,
        color: '#FFF'
    },
    grey: {
        backgroundColor: '#8c8c8c'
    },
    red: {
        backgroundColor: '#ff3333',
    },
    blue: {
        backgroundColor: '#4db8ff',
    },
    orange: {
        backgroundColor: '#ff8533',
    },
    icon: {
        fontSize: 25,
        color: 'white'
    }
});