import React, { useState } from 'react';
import {  Image as ReactImage, View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const Button = (props) => {

    const checkType = () => {
        var type = ('title');
        props.icon && props.title ? type = ('both') :
        props.icon != props.title ? type = ('icon'): null;
        return type;
    };

    const type = checkType();
     
   

    switch (type){
        case 'title' :
            return <TouchableOpacity {...props} style={ [Buttons.container]}>
            <Text style={Buttons.text}>{props.title}</Text>
            </TouchableOpacity>;
        case 'icon' :
            return <TouchableOpacity {...props} style={ [Buttons.container]}>
                <Icon name = {props.icon} style={Buttons.icon}/>
            </TouchableOpacity>;

        case 'both':
            return <TouchableOpacity {...props} style={ [Main.row, Buttons.container]}>
            <Icon name ={props.icon} style={Buttons.icon}/>
            <Text style={Buttons.text}>{props.title}</Text>
            </TouchableOpacity>;

        default:
            return <View><Text>Error, type not found</Text></View>;
    }
    


   
};

const Buttons = StyleSheet.create({
    container: {
        backgroundColor: '#22b5ff',
        borderRadius: 30,
        padding: 15,
        alignSelf: 'flex-end'
    },
    text: {
        fontSize: 20,
        color: '#FFF',
        paddingLeft: 10
    },
    icon: {
        fontSize: 25,
        color: 'white'
    }
});

export const AppView = (props) => {

    return <View style={{flex: 1}}>
        {props.children}
        <BottomMenu navigation={props.navigation}  />
        </View>
};


export const Image = (props) => {

    return <ReactImage 
    {...props}
    style={[{width: '100%'}, props.style]}/>
};

const Main = StyleSheet.create({
    row: {
        flexDirection: 'row'
    },
    shadow: {
        elevation: 8,
        shadowColor: '#d1d1d1',
        shadowOpacity: 0.3,
        shadowRadius: 1,
        shadowOffset: { width: 0, height: 2 },
    }
});
export const Card = ({ title, content, onShow, onDelete }) => {

    return (
        <View style={[Cards.container]}>
            <TouchableOpacity style={{flex: 1}}onPress={onShow}>
                <Text style={Cards.title}>{title}</Text>
                <Text style={Cards.content}>{content}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onDelete}>
            <Icon name={'delete'} style={BottomMenus.icon} />
            </TouchableOpacity>
        </View>
    );
};

const Cards = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flexDirection: 'row',
        justifyContent: 'space-between',

        margin: 10,
        padding: 20,
        borderRadius: 10,

    },
    title: {
        fontSize: 20,
        color: '#22b5ff'
    },
    content: {
        fontSize: 15,
        color: '#d1d1d1'
    }

});


export const BottomMenu = ({navigation}) => {
    const currentRoute = navigation.state.routeName;

    return (
        <View style={BottomMenus.container}>
            <TouchableOpacity onPress={() => { navigation.navigate('Liked') }}>
                <Icon name={'favorite'} style={currentRoute === 'Liked' ? BottomMenus.iconSelected : BottomMenus.icon} />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => { navigation.navigate('Index')}}>
                <Icon name={'home'} style={currentRoute === 'Index' ? BottomMenus.iconSelected : BottomMenus.icon} />
            </TouchableOpacity>

            {/* <TouchableOpacity onPress={() => { navigation.navigate('Archived'); }}>
                <Icon name={'delete'} style={currentRoute === 'Archived' ? BottomMenus.iconSelected : BottomMenus.icon} />
            </TouchableOpacity> */}


        </View>
    );
};

const BottomMenus = StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: '#FFF',
        borderRadius: 50,
        justifyContent: 'space-around',
        flexDirection: 'row',
        paddingTop: 20,
        paddingBottom: 15,
        width: '95%',

       
        bottom: 20,
        alignSelf: 'center',

        //shadow
        elevation: 10,
       

    },
    icon: {
        fontSize: 30,
        color: '#e7e7e7'
    },
    iconSelected: {
        fontSize: 40,
        color: '#22b5ff'
    }
});

export const Fab = ({ onPress }) => {

    return (
        <TouchableOpacity onPress={onPress} style={[Fabs.Fab, Main.shadow]}>
            <Icon name='add' style={Fabs.icon} />
        </TouchableOpacity>
    );
};

const Fabs = StyleSheet.create({
    Fab: {
        position: 'absolute',
        alignSelf: 'flex-end',
        backgroundColor: '#22b5ff',
        borderRadius: 100,
        height: 70,
        width: 70,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 140,
        right: 30,
        elevation: 1

    },
    icon: {
        fontSize: 30,
        color: 'white'
    }

});



