import React, {useState} from 'react';
import { View, Text, StyleSheet, TextInput  } from 'react-native';
import { Button} from '../components/MainComponents';
import { EditTags } from '../components/UI';

const BlogPostForm = ({onSubmit, initialValues }) => {
    const [title, setTitle] = useState(initialValues.title);
    const [content, setContent] = useState(initialValues.content);
    
    const tags = [
        {
          id: '1',
          title: 'to-do',
          color: 'orange'
        },
        {
          id: '2',
          title: 'important',
          color: 'red'

        },
        {
          id: '3',
          title: 'study',
          color: 'blue'

        },
      ];

    return (
        <View style={style.card}>
        <TextInput
        value={title} 
        placeholder={'Enter Title'} onChangeText={(text) => setTitle(text)} style={style.title} />
        <TextInput 
        value={content} 
        numberOfLines ={4}
        multiline
        placeholder='Enter Content'onChangeText={(text)=> setContent(text)} style={style.content} />
        <Button
        icon = {'save'}
        onPress={ () => onSubmit(title, content) }
  
            />
    </View>

    );

};


BlogPostForm.defaultProps = {
    initialValues: {
        title: '',
        content: '',
        tags: [      ]
    }

};

const style = StyleSheet.create({
    card: {
        backgroundColor: '#FFF',
        justifyContent: 'space-between',

        margin: 10,
        padding: 20,
        borderRadius: 10
    },
    title: {
        fontSize: 20,
        color: '#22b5ff'
    },
    content: {
        textAlignVertical: 'top',
        fontSize: 20,
        color: '#d1d1d1'
    }
});

export default BlogPostForm;