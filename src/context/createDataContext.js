import React, { useReducer } from 'react';

// Not too sure tbh but essentally built a function to automate
// alot of the Context/Provider set up to reduce code!
// It needs: reducer, the action you want to perform on reducer as well as some state you want to change

export default (reducer, actions, intialState) => {

    

    // 1. create Reducer
    const Context= React.createContext();

    // 2. create Provider and link it to Context and State
    const Provider = ({children}) => {
        const [state, dispatch] = useReducer(reducer, intialState);

        // actions === {addBlogPost: (dispatch) => {return ()=> {}}}

        const boundActions = {};
        for (let key in actions){

            // Honestly not sure but I think below:
            
            // key === 'addBlogPost'
            // the actions defined in our BlogProvider, we want to call he dispatch
            boundActions[key] = actions[key](dispatch);
            // equivalent to add boundActions.addBlogPost = actions.addBlogPost( (dispatch) => {} ) }
        }

        // 3. It must return a Provider, and pass the the value as props
        // 8. State and bound actions were passed here, we destructured of it. 
        return <Context.Provider value={{state, ...boundActions}}>
            {children}
        </Context.Provider>
    };

    //4. It returns the Context and Provider to be used in your custom context component e.g BlogContext
    return {Context, Provider};
};

