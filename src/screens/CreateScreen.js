import React, { useContext} from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import {Context} from '../context/BlogContext';
import BlogPostFrom from '../components/BlogPostFrom';

const CreateScreen = ({navigation}) => {
   
    const {addBlogPost} = useContext(Context);

    return (
        <BlogPostFrom

        onSubmit={(title, content) => { 
            addBlogPost(title, content, ()=> navigation.navigate('Index'));
        }}
        
        />
       
    );

};


const style = StyleSheet.create({
    title: {
        fontSize: 20,
        color: '#22b5ff'
    },
    content: {
        fontSize: 15,
        color: '#d1d1d1'
    }
    
});

CreateScreen.navigationOptions = (navigation) => {
    return {
        title: 'Create a Blog'
    }
};

export default CreateScreen;