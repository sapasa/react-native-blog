import React, { useEffect, useContext } from 'react';
import { View, Text, StyleSheet, FlatList, Button, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AppView, Image, BottomMenu, Fab, Card } from '../components/MainComponents';
import { Context } from '../context/BlogContext';


const IndexScreen = ({ navigation }) => {

    const { state, deleteBlogPost, getBlogPosts } = useContext(Context);

    useEffect(() => {
        getBlogPosts();

        const listener = navigation.addListener('willFocus', () => {
            console.log('Not working supposed to call on');
            getBlogPosts();
        });

        return () => {
            listener.remove();
        };


    }, []);

    return (
        <AppView navigation={navigation} style={{ flex: 1 }}>
            <FlatList
                contentContainerStyle={{ paddingBottom: 130 }}
                data={state}
                keyExtractor={(state) => state.id.toString()}
                renderItem={({ item }) => {
                    return (
                        <Card
                            onDelete={() => deleteBlogPost(item.id)}
                            onShow={() => navigation.navigate('Show', { id: item.id })}
                            title={item.title}
                            content={item.content}
                        />
                    );
                }}
            />
            <Fab onPress={() => navigation.navigate('Create')} />
        </AppView>

    );

};


export default IndexScreen;