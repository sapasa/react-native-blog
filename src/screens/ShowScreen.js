import React, { useState, useContext } from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Context } from '../context/BlogContext';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {BottomMenu} from '../components/MainComponents';

const ShowScreen = ({ navigation }) => {
    const id = (navigation.getParam('id'));

    const { state } = useContext(Context);
    const blogPost = state.find((blogPost) => blogPost.id === navigation.getParam('id'));

    return (
        <View style={{ flex: 1 }}>
            <Image source={ require('../assets/Yegor-Meteor-Planet.png')} style={style.image} />
            <View style={style.card}>
                <Text style={style.title}>{blogPost.title} </Text>
                <Text style={style.content}>{blogPost.content} </Text>
                <View style={style.iconsContainer}>
                    <TouchableOpacity onPress={ () =>navigation.navigate('Edit', {id: navigation.getParam('id')}) }>
                        <Icon name={'edit'} style={style.icon} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ () => console.log('Need to code a favourites function') }>
                        <Icon name={'favorite'} style={style.icon} />
                    </TouchableOpacity>
                </View>
            </View>
            <BottomMenu navigation={navigation} />
        </View>


    );

};



const style = StyleSheet.create({
    image: {
        height: 400,
        width: '100%',

    },
    card: {
        padding: 30,
        backgroundColor: '#FFF',
        borderRadius: 30,
        flex: 1,
        width: '100%',
        bottom: 30,
      
    },
    title: {
        fontSize: 20,
        color: '#22b5ff'
    },
    content: {
        paddingTop: 10,
        fontSize: 20,
        color: '#d1d1d1',
        paddingRight: 50
    },
    iconsContainer: {
        alignSelf: 'flex-end',
        top: -30,
        right: 20,
        elevation: 4,
        position: 'absolute',
        backgroundColor: '#FFF',
        borderRadius: 50,
    },
    iconContainer: {
        alignSelf: 'flex-end',
        top: -30,
        right: 20,
        elevation: 4,
        position: 'absolute',
        backgroundColor: '#FFF',
        borderRadius: 50,
    },
    icon: {
        fontSize: 25,
        color: '#22b5ff',
        padding: 20, 
       
    }
});

export default ShowScreen;