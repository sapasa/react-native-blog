import React, { useContext } from 'react';
import { View, Text, StyleSheet, FlatList, Button, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { BottomMenu, Fab, Card, AppView } from '../components/MainComponents';

const LikedScreen = ({ navigation }) => {
    return (
        <AppView navigation={navigation} style={{ flex: 1 }}>
            <View style={[{flex:1}, style.parent]}>
                <Text style={style.title}>Favourites</Text>
            </View>
        </AppView>
    );

};


const style = StyleSheet.create({
    parent: {
        paddingTop: 30,
        paddingLeft: 30
    },
    title: {
        fontSize: 20,
        color: '#22b5ff'
    },
    content: {
        fontSize: 15,
        color: '#d1d1d1'
    }
});

export default LikedScreen;